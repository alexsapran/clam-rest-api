install:
	go install 
bin:
	@mkdir -p releases
	go get -v . && go build -o releases/clam-rest-api

test:
	@go get -v . 
	@go vet && go test -v .

container: build
	@docker build -f Dockerfile.release.yaml -t alexsapran/clam-rest-api:latest .

container-onbuild:
	@docker build -t alexsapran/clam-rest-api:onbuild .

push: container
	@docker push alexsapran/clam-rest-api

build: test
	go get -v . 														&& \
	mkdir -p releases													&& \
	GOARCH=386 GOOS=linux   go build -o releases/clam-rest-api-linux		&& \
	GOARCH=386 GOOS=windows go build -o releases/clam-rest-api-win.exe	&& \
	GOARCH=386 GOOS=darwin  go build -o releases/clam-rest-api-darwin

clean:
	rm -rf ./releases | true

forma:
	@go fmt .
