# Clam TCP to Rest API

This is a sample project of how you can use go with the help of this library `github.com/dutchcoders/go-clamd` in order
to create a rest client for the clam antivirus server that can be found here https://hub.docker.com/r/peopleperhour/clam/

This is a simple rest server, more as a proof of concept, and the goal of the project is to learn some go! (apparently) and to show how some can use
the clam antivirus server.

This project is not intended for production use.

## Envrionment variables
In order for the rest client to start you need to set 2 environment variables `CLAM_SERVER` and `CLAM_PORT` (optional, default:3310)

## Endpoints

The default web controller is listening on port `6000`

### Root
This is only to check that the web part is up. This should not be used as a health check.

```
Path : /
Method: GET
Response status: 200
Response body:
    Clam TCP to rest client
```

### Health
This is the health check that a load balancer should check. It validates that the server can indeed execute a remote TCP
ping to the clam server.

#### Healthy example:
```
Path : /health
Method: GET

Response headers:
    HTTP/1.1 200 OK
    Content-Type: application/json
    Date: Fri, 10 Mar 2017 10:54:30 GMT
    Content-Length: 2
Response body:
    ok
```

#### Unhealthy example:
```
Path : /health
Method: GET

Response headers:
    HTTP/1.1 503 Service Unavailable
    Content-Type: text/plain; charset=utf-8
    X-Content-Type-Options: nosniff
    Date: Fri, 10 Mar 2017 10:55:05 GMT
    Content-Length: 20
Response body:
    Service Unavailable
```


### Version
Get information about the remote clam server version

```
Path : /version
Method: GET

Response headers:
    HTTP/1.1 200 OK
    Content-Type: application/json
    Date: Fri, 10 Mar 2017 10:53:42 GMT
    Content-Length: 102

Response body:
    {"command":"version","message":"ClamAV 0.99.2/22558/Fri Nov 18 11:16:33 2016","file":"","metadata":""}
```

### Stats
Get statistics of the clam server

```
Path : /stats
Method: GET

Response headers:
    HTTP/1.1 200 OK
    Date: Fri, 10 Mar 2017 10:56:51 GMT
    Content-Length: 267
    Content-Type: text/plain; charset=utf-8

Response body:
    {"Pools":"1","State":"STATE: VALID PRIMARY","Threads":"THREADS: live 1  idle 0 max 15 idle-timeout 120","Memstats":"MEMSTATS: heap 5.098M mmap 0.129M used 3.765M free 1.336M releasable 0.055M pools 1 pools_used 417.095M pools_total 417.112M","Queue":"QUEUE: 0 items"}
```

### Scan
Scan a remote file from a URI. This operation has the logic of submiting a file to be scanned and when complete (in a seperate go routine) then
the application will send back to a notify url the results.

```
Path : /scan?file=...&pingback=...
Method: GET

Response headers:
    HTTP/1.1 200 OK
    Content-Type: application/json
    Date: Fri, 10 Mar 2017 11:00:30 GMT
    Content-Length: 171

Response body:
    {"command":"scan","message":"Queued","file":"....","metadata":""}
```

When the file is scanned you will get the results in your pingback url that defined in the submit of the file.

The results will send as a POST.
Example :
```
{"command":"scan","message":"Eicar-Test-Signature","file":".....160329143458_eicar.com.zip","metadata":""}
```