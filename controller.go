package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dutchcoders/go-clamd"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type WebController struct {
	clamUrl string
}

func (controller WebController) Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Clam TCP to rest client!\n")
}

func (controller WebController) Health(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	clam := clamd.NewClamd(controller.clamUrl)
	err := clam.Ping()
	if err == nil {
		fmt.Fprint(w, "ok")
	} else {
		http.Error(w, "Service Unavailable", 503) // Or Redirect?
	}
}

func (controller WebController) Version(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	clam := clamd.NewClamd(controller.clamUrl)
	v, _ := clam.Version()
	version := <-v
	response, _ := json.Marshal(ClamResponse{Command: "version", File: "", Message: version.Raw, Metadata: ""})
	fmt.Fprint(w, string(response))
}

func (controller WebController) Scan(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	file := r.URL.Query().Get("file")
	pingback := r.URL.Query().Get("pingback")
	meta := r.URL.Query().Get("meta")
	if file != "" && pingback != "" {
		go controller.processFile(file, pingback)
		response, _ := json.Marshal(ClamResponse{Command: "scan", File: file, Message: "Queued", Metadata: meta})
		fmt.Fprint(w, string(response))
	} else {
		fmt.Fprint(w, "Empty file or pingback url as an input, you must send file url and pingback parameter")
	}
}

func (controller WebController) processFile(file string, pingback string) {
	response, err := http.Get(file)
	if err != nil {
		log.Fatal(err)
		rsp, _ := json.Marshal(ClamResponse{Command: "error", Message: "Cloud not fetch file", File: file})
		controller.sendReport(pingback, rsp)
		return
	}
	defer response.Body.Close()
	clam := clamd.NewClamd(controller.clamUrl)
	chanFoo := make(chan bool)

	ch, _ := clam.ScanStream(response.Body, chanFoo)
	x := <-ch
	rsp, _ := json.Marshal(ClamResponse{Command: "scan", Message: x.Description, File: file})
	time.Sleep(2 * time.Second)
	controller.sendReport(pingback, rsp)
}

func (controller WebController) sendReport(pingback string, report []byte) {
	log.Println(string(report))

	fmt.Println("URL:>", pingback)

	req, err := http.NewRequest("POST", pingback, bytes.NewBuffer(report))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error: %v", err)
		return
	}
	defer resp.Body.Close()

	fmt.Printf("response Status: %v", resp.Status)
	fmt.Printf("response Headers: %v", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}

func (controller WebController) Stats(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	clam := clamd.NewClamd(controller.clamUrl)
	stats, _ := clam.Stats()
	response, _ := json.Marshal(stats)
	fmt.Fprint(w, string(response))
}

type ClamResponse struct {
	Command  string `json:"command"`
	Message  string `json:"message"`
	File     string `json:"file"`
	Metadata string `json:"metadata"`
}
