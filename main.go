package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"os"
)

func main() {

	clamServer := os.Getenv("CLAM_SERVER")
	if clamServer == "" {
		log.Fatal("Missing environment host for the clam server. Please set CLAM_SERVER")
	}
	clamPort := os.Getenv("CLAM_PORT")
	if clamPort == "" {
		clamPort = "3310"
	}
	clamUri := fmt.Sprintf("tcp://%s:%s", clamServer, clamPort)

	router := httprouter.New()
	controller := WebController{clamUri}

	router.GET("/", controller.Index)
	router.GET("/version", controller.Version)
	router.GET("/stats", controller.Stats)
	router.GET("/scan", controller.Scan)
	router.GET("/health", controller.Health)

	log.Println("Service is starting...")
	log.Fatal(http.ListenAndServe(":6000", router))
}
